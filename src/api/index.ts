import { DeckDefinition } from "../utils/deckFormatConverter";

const api = "https://cerny-rytir-api.vrba.dev";

export const loadDeck = async (definition: DeckDefinition): Promise<DeckResponse> => {
    return fetch(
        `${api}/api/scrape/deck`, {
        method: "POST",
        body: JSON.stringify({cards: definition}),
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    })
    .then(response => response.json());
}

export const loadCard = async (card: string): Promise<CardResponse> => {
    return fetch(
        `${api}/api/scrape/card`, {
        method: "POST",
        body: JSON.stringify({input: card}),
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    })
    .then(response => response.json());
}

export type DeckResponse = {
    cards: Array<CardResponse>;
    median_deck_price: number;
    all_cards_in_stock: boolean;
};

export type CardResponse = {
    input_query: string,
    cards: Array<ScrapedCardResource>
    median_price: number;
    requested_count: number;
    total_pieces_in_stock: number;
    all_cards_in_stock: boolean;
};

export type ScrapedCardResource = {
    card_name: string;
    card_type: string;
    card_image_url: string;
    card_set_name: string;
    card_set_icon: string;
    card_rarity: string;
    card_price: number;
    card_pieces_in_stock: number;
};
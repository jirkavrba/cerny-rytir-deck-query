import { FC, useState } from "react";
import { CardResponse, loadCard } from "../api";

export type CardResultProps = {
    response: CardResponse;
    overridden: boolean;
    onCardLoaded: (card: string, response: CardResponse) => void;
};

export const CardResult: FC<CardResultProps> = ({ response, overridden, onCardLoaded }) => {
    const [loading, setLoading] = useState(false);
    const containsNoCards = response.cards.length === 0;

    const loadCardsNotInStock = async () => {
        setLoading(true);

        try {
            const card = response.input_query;
            const cardResponse = await loadCard(card);

            onCardLoaded(card, cardResponse);
        }
        catch {
            window.alert("Při načítání karet došlo k chybě!");
        }
        finally {
            setLoading(false);
        }
    }


    return (
        <div className={`flex flex-col gap-2 p-4 rounded-xl z-0 ${response.all_cards_in_stock ? "bg-neutral-100" : "bg-red-200"}`}>
            <div className="flex flex-row items-start justify-between">
                <h1 className="font-black">{response.requested_count} &times; {response.input_query}</h1>
                <div className="ml-2">
                    {!overridden && (
                        <button className="bg-neutral-500 uppercase font-bold tracking-wider rounded-xl p-2 text-xs text-white" disabled={loading} onClick={loadCardsNotInStock}>
                            {loading ? "Hledám na cernyrytir.cz..." : "Vyhledat i karty co nejsou skladem"}
                        </button>
                    )}
                </div>
            </div>
            {response.all_cards_in_stock ? (
                <div className="text-xs font-mono text-neutral-500 font-bold">
                    {response.requested_count} &times; {response.median_price} Kč = {response.median_price * response.requested_count} Kč (medián)
                </div>
            ) : (
                <div className="flex flex-col items-start gap-2">
                    <div className="text-red-800 font-bold text-xs">
                        V decku je karta {response.requested_count}&times;, skladem je pouze {response.total_pieces_in_stock} ks!
                    </div>
                </div>
            )}
            <div className="flex flex-row flex-wrap gap-4">
                {containsNoCards && (
                    <div className="flex flex-col items-start gap-2">
                        <div className="text-red-500 font-bold">Na cernyrytir.cz nejsou žádné karty skladem!</div>
                    </div>
                )}
                {response.cards.map((card, i) => (
                    <div key={i} className={`flex flex-row items-start justify-center gap-4 bg-white p-4 rounded-xl ${card.card_pieces_in_stock === 0 ? "transition opacity-50 hover:opacity-100" : ""}`}>
                        <a href={card.card_image_url} target="_blank">
                            <img src={card.card_image_url} className="h-24 rounded" />
                        </a>
                        <div className="flex flex-col">
                            <div className="font-bold">{card.card_name}</div>
                            <div className="flex flex-row items-center justify-start gap-1">
                                <img src={card.card_set_icon} className="w-4 h-4" />
                                <span className="text-neutral-500 text-xs">
                                    {card.card_set_name}
                                </span>
                            </div>

                            <div className={`font-mono font-bold mt-2 text-sm ${card.card_pieces_in_stock === 0 ? "text-red-800" : ""}`}>{card.card_pieces_in_stock} ks skladem</div>
                            <div className="font-mono text-sm">{card.card_price} Kč</div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};
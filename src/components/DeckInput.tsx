import { FC, useState } from "react";

export type DeckInputProps = {
    className?: string;
    onSubmit: (deck: string) => void;
};

export const DeckInput: FC<DeckInputProps> = ({ className = "", onSubmit }) => {
    const [value, setValue] = useState("");
    const submit = () => {
        if (value.trim().length > 0) {
            onSubmit(value);
        }
    };

    return (
        <div className={`${className} flex flex-col p-4 gap-4 rounded-xl bg-purple-100`}>
            <textarea 
                className="flex-grow rounded-lg p-2 text-xs font-mono resize-none" 
                placeholder="Deck source (Arena format)" 
                value={value} 
                onChange={(event) => setValue(event.target.value)}
            />
            <button className="bg-purple-500 text-white p-4 rounded-xl uppercase font-black tracking-wide" onClick={submit}>
                Import
            </button>
        </div>
    );
};
import { FC } from "react";
import { CardResponse, DeckResponse } from "../api";
import { CardResult } from "./CardResult";

export type DeckResultProps = {
    loading: boolean;
    loadedDeck?: DeckResponse | null;
    loadedCards?: Record<string, CardResponse>,
    className?: string;
    onCardLoaded: (card: string, response: CardResponse) => void;
};

export const DeckResult: FC<DeckResultProps> = ({ loading, loadedDeck = null, loadedCards = {}, className = "", onCardLoaded }) => {
    if (!loadedDeck && !loading) {
        return (
            <div className={`${className} flex flex-row items-center justify-center text-neutral-400`}>
                &larr; Nalevo si naimportuj deck v MTG Arena formátu
            </div>
        )
    }

    if (loading) {
        return (
            <div className={`${className} flex flex-col gap-2 items-center justify-center`}>
                <h1 className="font-bold">Hledám karty na cernyrytir.cz...</h1>
                <img src="/loading.gif" className="rounded-xl" />
            </div>
        )
    }

    if (!loadedDeck) {
        return <></>;
    }

    const cards = loadedDeck.cards;
    const filtered = cards.filter(response => !response.cards.some(it => it.card_type.toLowerCase().includes("basic land")))

    return (
        <div className={`${className} max-h-screen flex flex-col gap-4 overflow-y-scroll pr-4 z-0`}>
            {
                loadedDeck.all_cards_in_stock
                    ? (
                        <>
                            <div className="bg-green-500 text-white font-bold rounded-xl px-4 py-2">
                                <div>
                                    Všechny karty z decku jsou na cernyrytir.cz skladem.
                                </div>
                                <div className="text-green-100 text-normal text-sm">
                                    Algoritmus (zatím) nekontroluje počet naskladněných karet vůči decklistu.
                                </div>
                            </div>

                            <div className="bg-purple-500 text-white font-bold rounded-xl px-4 py-2">
                                <div className="text-purple-100">
                                    Cena celého decku (vypočtená z mediánu cen karet, pouze orientační)
                                </div>
                                <div className="text-2xl font-black tracking-wide">
                                    {loadedDeck.median_deck_price} Kč
                                </div>
                            </div>
                        </>
                    )
                    : (
                        <>
                            <div className="bg-red-500 text-white font-bold rounded-xl px-4 py-2">
                                <div>
                                    Deck obsahuje karty co nejsou na cernyrytir.cz skladem!
                                </div>
                            </div>

                            <div className="bg-purple-100 font-bold rounded-xl px-4 py-2">
                                <div className="text-purple-400">
                                    Cena dostupných karet (vypočtená z mediánu cen karet, pouze orientační)
                                </div>
                                <div className="text-2xl font-black tracking-wide text-purple-500">
                                    {loadedDeck.median_deck_price} Kč
                                </div>
                            </div>
                        </>
                    )
            }

            <div className="bg-red-100 text-red-800 font-bold rounded-xl px-4 py-2">
                Ve výsledcích nejsou zobrazeny basic landy.
            </div>

            {filtered.map((response, i) => {
                const override = loadedCards[response.input_query];
                const base = override || response;
                const overridden: CardResponse = {
                    ...base,
                    requested_count: response.requested_count,
                    all_cards_in_stock: response.all_cards_in_stock
                };

                return (
                    <CardResult
                        key={i}
                        response={overridden}
                        overridden={!!override}
                        onCardLoaded={onCardLoaded}
                    />
                );
            })}
        </div>
    )
};
import { FC, useState } from "react";
import { DeckInput } from "./DeckInput";
import { DeckResult } from "./DeckResult";
import { mtgArenaToDeckDefinition } from "../utils/deckFormatConverter";
import { CardResponse, DeckResponse, loadDeck } from "../api";

export const DeckView: FC = () => {
    const [loading, setLoading] = useState(false);
    const [loadedDeck, setLoadedDeck] = useState<DeckResponse | null>(null);
    const [loadedCards, setLoadedCards] = useState<Record<string, CardResponse>>({});

    const handleDeckInput = async (deck: string) => {
        setLoading(true);

        try {
            const definition = mtgArenaToDeckDefinition(deck);
            const response = await loadDeck(definition);

            setLoadedDeck(response);
            setLoadedCards({});
        }
        catch {
            window.alert("Při načítání decku došlo k chybě!")
        }
        finally {
            setLoading(false);
        }
    };

    return (
        <div className="h-screen p-4 flex flex-row flex-grow items-stretch rounded-xl gap-4">
            <DeckInput className="flex-[1]" onSubmit={handleDeckInput} />
            <DeckResult
                className="flex-[3]"
                loading={loading}
                loadedDeck={loadedDeck}
                loadedCards={loadedCards}
                onCardLoaded={(card, response) => setLoadedCards(previous => ({ ...previous, [card]: response }))}
            />
        </div>
    );
};
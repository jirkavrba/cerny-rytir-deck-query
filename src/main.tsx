import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { DeckView } from './components/DeckView'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <DeckView />
  </React.StrictMode>,
)

export type DeckDefinition = Record<string, number>

export const mtgArenaToDeckDefinition = (input: string): DeckDefinition => {
    const lines = input.trim().split("\n");
    const matches = lines.map(it => {
        const result = it.match(/^(\d+)\s+(.*)(\s+\([A-Z0-9]{3}\)\s+\d+)$/) || it.match(/^(\d+)\s+(.*)$/);

        if (result === null) {
            return null;
        }

        const [, count, name] = result;
        return { count, name };
    });

    return matches
        .filter(it => it !== null)
        .reduce((acc, current) => {
            return {...acc, [current!.name]: Number(current!.count)}
        }, {});
};